# Image Downloads
[Back To Guide](README.md#creation)

[Back To Presentation](guide-presentation/creation/index.md)

## Debian
[Back To Top](#image-downloads)


| Version | Mirror |
| :-- | :-- |
| Used In _Guide_ | [**Debian 11.1.0 AMD64**](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.1.0-amd64-netinst.iso) |
